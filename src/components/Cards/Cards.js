import React from "react";
import CardItem from "./CardItem";

//Using function (stateless) component when using simple html component
function Cards() {
  return (
    <div className="cards">
      <div className="cards__container">
        <h2>Browse our categories</h2>
        <div className="cards__wrapper">
          <ul className="cards__items">
            <CardItem
              src="https://image.coolblue.nl/transparent/max/310x275/content/518557"
              text="Computers & tablets"
              path="/products"
            />
            <CardItem
              src="https://image.coolblue.nl/transparent/max/310x275/content/518564"
              text="Image & sound"
              path="/products"
            />
            <CardItem
              src="https://image.coolblue.nl/transparent/max/310x275/content/518559"
              text="Phones"
              path="/products"
            />
            <CardItem
              src="https://image.coolblue.nl/transparent/max/310x275/content/518565"
              text="Home & Living"
              path="/products"
            />
            <CardItem
              src="https://image.coolblue.nl/transparent/max/310x275/content/518562"
              text="Kitchen"
              path="/products"
            />
            <CardItem
              src="https://image.coolblue.nl/transparent/max/310x275/content/518952"
              text="Sports & beauty"
              path="/products"
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
