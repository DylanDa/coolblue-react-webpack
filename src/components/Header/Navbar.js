import React from "react";
import { NavLink } from "react-router-dom";

import coolblueNavbarLogo from "../../assets/images/coolblue_navbar.svg";

//Using class component (stateful) when more using more advanced state
class Navbar extends React.Component {
  constructor(props) {
    super(props);

    //Components states
    this.state = {
      mobileMenuDisplayed: false,
      navbarDeployed: false,
    };
  }

  //Component logic methods
  handleMobileMenuClick = () => {
    this.setState((state) => ({
      mobileMenuDisplayed: !state.mobileMenuDisplayed,
    }));
  };
  closeMobileMenu = () => {
    this.setState({ mobileMenuDisplayed: false });
  };
  changeNavbarBackground = () => {
    if (window.scrollY >= 80) {
      this.setState({ navbarDeployed: true });
    } else {
      this.setState({ navbarDeployed: false });
    }
  };

  //Components lifecycle methods
  componentDidMount() {
    window.addEventListener("scroll", this.changeNavbarBackground);
  }

  render() {
    return (
      <>
        <nav
          className={
            this.state.navbarDeployed ? "navbar navbar--deployed" : "navbar"
          }
        >
          <div className="navbar-container">
            <NavLink to="/" className="navbar-logo">
              <img
                className="navbar-logo-img"
                src={coolblueNavbarLogo}
                alt="Coolblue logo"
              />
            </NavLink>
            <div
              className={
                this.state.mobileMenuDisplayed
                  ? "menu-icon menu-icon--opened"
                  : "menu-icon menu-icon--closed"
              }
              onClick={this.handleMobileMenuClick}
            >
              <i
                className={
                  this.state.mobileMenuDisplayed
                    ? "menu-icon-item fas fa-times"
                    : "menu-icon-item fas fa-bars"
                }
              ></i>
            </div>
            <ul
              className={
                this.state.mobileMenuDisplayed ? "nav-menu active" : "nav-menu"
              }
            >
              <li className="nav-item">
                <NavLink
                  to="/"
                  className="nav-link"
                  exact={true}
                  activeClassName="nav-link--active"
                  onClick={this.closeMobileMenu}
                >
                  Home
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to="/products"
                  className="nav-link"
                  activeClassName="nav-link--active"
                  onClick={this.closeMobileMenu}
                >
                  Products
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      </>
    );
  }
}

export default Navbar;
