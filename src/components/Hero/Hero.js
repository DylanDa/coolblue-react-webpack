import React from "react";

import video from "../../assets/videos/hero.mp4";

//Using function (stateless) component when using simple html component
function Hero() {
  return (
    <div className="hero-container">
      <video className="hero-video" src={video} autoPlay loop muted />
      <h1>COOLBLUE ADVENTURE</h1>
      <p>With a smile.</p>
    </div>
  );
}

export default Hero;
