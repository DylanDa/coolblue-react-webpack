import React from "react";
import Searchbar from "../Inputs/Searchbar";
import ProductItem from "./ProductItem";

import Loader from "react-loader-spinner";

class Products extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchEntry: "",
      products: [],
      loading: false,
    };
  }

  fetchData = async () => {
    this.setState({ loading: true });
    return await fetch(
      `http://localhost:3000/products?q=${this.state.searchEntry}&_limit=12`
    )
      .then((response) => response.json())
      .then((data) => {
        console.log("data fetched", data);
        this.setState({ products: data });
        this.setState({ loading: false });
      });
  };

  searchbarUpdated = (search) => {
    if (search.trim() != "") {
      this.setState({ searchEntry: search });
      this.setState({ products: this.fetchData() });
    } else {
      this.setState({ searchEntry: "" });
      this.setState({ products: [] });
    }
  };

  render() {
    return (
      <>
        <Searchbar handleChange={this.searchbarUpdated} />

        <Loader
          className="products__loader"
          type="ThreeDots"
          color="#e6e6e6"
          height={100}
          width={100}
          timeout={false}
          visible={this.state.loading}
        />

        <div className="products__results__container">
          {this.state.products.length ? (
            <div className="products-list__wrapper">
              <ul className="products__items">
                {this.state.products.map((product) => (
                  <ProductItem key={product.sku} product={product} />
                ))}
              </ul>
            </div>
          ) : (
            <i className="products__bg-icon fas fa-shopping-basket"></i>
          )}
        </div>
      </>
    );
  }
}

export default Products;
