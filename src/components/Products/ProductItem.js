import React from "react";
import { Link } from "react-router-dom";

function ProductItem(props) {
  return (
    <>
      <li className="product__item">
        <Link className="product__item__link" to="">
          <div className="product__item__img-wrap">
            <img className="product__item__img" src={props.product.image} />
          </div>
          <div className="product__item__info">
            <h5 className="product__item__text">{props.product.name}</h5>
          </div>
        </Link>
      </li>
    </>
  );
}

export default ProductItem;
