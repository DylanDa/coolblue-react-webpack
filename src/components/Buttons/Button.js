import React from "react";

//Using function (stateless) component when using simple html component
function Button(props) {
  const className = `btn ${props.type}`;

  return (
    <button className={className} onClick={props.handleClick}>
      {props.icon && props.icon} {props.label}
    </button>
  );
}

export default Button;
