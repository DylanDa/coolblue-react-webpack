import React, { Component } from "react";

export class Searchbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchEntry: "",
    };

    //Timer
    this.typingTimeout = null;

    //Event
    this.onFieldChange = this.onFieldChange.bind(this);
  }

  onFieldChange(event) {
    // Clears the previously set timer.
    clearTimeout(this.typingTimeout);

    this.setState({ searchEntry: event.target.value });

    // Reset the timer, to make the http call after 475MS
    this.typingTimeout = setTimeout(() => {
      this.props.handleChange(this.state.searchEntry);
    }, 475);
  }

  render() {
    return (
      <div className="products__searchbar__container">
        <div className="products__searchbar__wrap">
          <input
            name="products-searchbar"
            className="products__searchbar"
            placeholder="Search products..."
            type="text"
            onChange={this.onFieldChange}
          />
          <i className="products__searchbar__icon fas fa-search"></i>
        </div>
      </div>
    );
  }
}

export default Searchbar;
