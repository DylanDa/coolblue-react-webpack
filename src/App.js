import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

//Styles
import "./styles/main.scss";

//Components
import Navbar from "./components/Header/Navbar";

//Pages
import HomePage from "./pages/HomePage";
import ProductsPage from "./pages/ProductsPage";

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/products" exact component={ProductsPage} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
