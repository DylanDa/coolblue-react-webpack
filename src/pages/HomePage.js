import React from "react";

import Cards from "../components/Cards/Cards";
import Footer from "../components/Footer/Footer";
import Hero from "../components/Hero/Hero";

function HomePage() {
  return (
    <>
      <Hero />
      <Cards />
      <Footer />
    </>
  );
}

export default HomePage;
